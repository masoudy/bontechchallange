package httpGateway.infrastructure

import eventSystem.*
import kotlin.reflect.KClass

class EventStoreRabbitMQImpl:EventStore {
    override fun fetchStream(stream: EventStreamName): EventStream {
        TODO("Not yet implemented")
    }

    override fun appendToStreamAndPublishChanges(
        streamName: EventStreamName,
        expectedVersion: Int,
        changes: List<Event>
    ) {
        TODO("Not yet implemented")
    }

    override fun appendToStreamAndPublishChanges(streamName: EventStreamName, changes: List<Event>) {
        TODO("Not yet implemented")
    }

    override fun <T : KClass<out Event>> subscribeToPublishedEventsOfTypes(
        vararg types: T,
        handler: SubscriptionHandler<Event>
    ) {

    }

    override fun <T : Event> fetchStreamOf(): EventStream {
        TODO("Not yet implemented")
    }
}