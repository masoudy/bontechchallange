package httpGateway


import eventSystem.Commander
import httpGateway.infrastructure.EventStoreRabbitMQImpl
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import reports.ReportsHelper
import usecase.FinancialBCUsecaseHelper
import usecase.ServicesBCUsecaseHelper
import usecase.UserBCUsecaseHelper
import javax.annotation.PostConstruct


@SpringBootApplication
class ServerStartup
{
    @Bean
    fun eventStore() = EventStoreRabbitMQImpl()

    @Bean
    fun commander() = Commander(eventStore())

    @PostConstruct
    fun initialize()
    {
        val eventStore = eventStore()
        UserBCUsecaseHelper.initialize(eventStore)
        FinancialBCUsecaseHelper.initialize(eventStore)
        ServicesBCUsecaseHelper.initialize(eventStore)
        ReportsHelper.initialize(eventStore)
    }
}

fun main(args: Array<String>) {

    runApplication<ServerStartup>(*args)
}