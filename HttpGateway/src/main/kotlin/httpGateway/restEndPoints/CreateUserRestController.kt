package httpGateway.restEndPoints

import eventSystem.Commander
import eventSystem.commands.CreateUserCommand
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/Users")
@RestController
class CreateUserRestController {

    @Autowired
    lateinit var commander:Commander

    @PostMapping
    fun createUser()
    {
        val userNameReceivedFromHeaderOrQueryString = ""
        val passwordReceivedFromHeaderOrQueryString = ""

        commander.sendCommand(CreateUserCommand(userNameReceivedFromHeaderOrQueryString,passwordReceivedFromHeaderOrQueryString))
    }

}