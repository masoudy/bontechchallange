package eventSystem.commands

import eventSystem.CommandEvent

class DisqualifyUserFromRoleCommand(val userId:String, val roleName:String):CommandEvent("DisqualifyUserFromRole")