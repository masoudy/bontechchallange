package eventSystem.commands

import eventSystem.CommandEvent

class WithdrawMoneyFromAccountCommand(val amount:Int, val unit:String, val userId:String):CommandEvent("WithdrawMoneyFromAccount")