package eventSystem.commands

import eventSystem.CommandEvent

class DepositMoneyToAccountCommand(val amount:Int,val unit:String,val userId:String):CommandEvent("DepositMoneyToAccount")