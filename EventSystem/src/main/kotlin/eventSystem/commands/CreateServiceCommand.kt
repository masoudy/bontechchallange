package eventSystem.commands

import eventSystem.CommandEvent

class CreateServiceCommand(val oneTimeUsagePrice:Int,val priceUnit:String,val serviceName:String):CommandEvent("CreateService")