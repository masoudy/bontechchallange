package eventSystem.commands

import eventSystem.CommandEvent

class QualifyUserForRoleCommand(val userId:String , val roleName:String):CommandEvent("QualifyUserForRole")