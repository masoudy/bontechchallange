package eventSystem

import kotlin.reflect.KClass


interface EventStore {
    fun fetchStream(stream: EventStreamName): EventStream
    fun appendToStreamAndPublishChanges(streamName: EventStreamName, expectedVersion: Int, changes: List<Event>)
    fun appendToStreamAndPublishChanges(streamName: EventStreamName, changes: List<Event>)

    fun appendToStreamAndPublishChanges(streamName: EventStreamName, change: Event)
    {
        appendToStreamAndPublishChanges(streamName, listOf(change))
    }


    fun <T:KClass<out Event>> subscribeToPublishedEventsOfTypes(vararg types:T,handler: SubscriptionHandler<Event>)

    fun <T:Event> fetchStreamOf():EventStream
}

inline fun <reified T:Event> EventStore.subscribeToPublishedEventsOfType(handler: SubscriptionHandler<T>)
{
    subscribeToPublishedEventsOfTypes(T::class){
        handler.handle(it as T)
    }
}

fun interface SubscriptionHandler<E>
{
    fun handle(event:E)
}