package eventSystem

import java.util.*

open class Event(val name:String)
{
    val id:String = UUID.randomUUID().toString()
}