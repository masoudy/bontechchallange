package eventSystem.events

import eventSystem.Event

class UserWasDisqualifiedFromRoleEvent(val userId:String, val roleName:String):Event("UserWasDisqualifiedFromRole")