package eventSystem.events

import eventSystem.Event

class DepositingMoneyToAccountFailedEvent(val commandId:String):Event("DepositingMoneyToAccountFailed")