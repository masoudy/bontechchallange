package eventSystem.events

import eventSystem.Event

class UserWasQualifiedForRoleEvent(val userId:String, val roleName:String):Event("UserWasQualifiedForRole")