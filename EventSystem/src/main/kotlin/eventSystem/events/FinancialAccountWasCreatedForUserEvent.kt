package eventSystem.events

import eventSystem.Event

class FinancialAccountWasCreatedForUserEvent(val userId: String, val accountId: String) :Event("FinancialAccountWasCreatedForUser") {
}