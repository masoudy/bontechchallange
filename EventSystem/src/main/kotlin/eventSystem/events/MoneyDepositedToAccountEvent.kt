package eventSystem.events

import eventSystem.Event

class MoneyDepositedToAccountEvent(val accountId:String, val amount:Int, val unit:String):Event("MoneyDepositedToAccount") {
}