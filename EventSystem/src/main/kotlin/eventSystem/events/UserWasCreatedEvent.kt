package eventSystem.events

import eventSystem.Event

class UserWasCreatedEvent(val userId:String, val password:String):Event("UserWasCreated")