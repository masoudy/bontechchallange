package eventSystem.events

import eventSystem.Event

class RoleWasCreatedEvent(val roleName:String):Event("RoleWasCreated")