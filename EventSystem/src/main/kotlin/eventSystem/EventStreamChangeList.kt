package eventSystem

data class EventStreamChangeList(val streamName: EventStreamName,val currentExpectedVersion:Int,val changes:List<Event>)