package domin.entity

import domin.valueObject.ActivePeriodId
import domin.valueObject.UsageCountThreshold
import eventSystem.Event
import java.util.*

class ServiceActivePeriod private constructor(events: List<Event>):Entity(events)
{

    lateinit var id:ActivePeriodId
        private set

    lateinit var threshold: UsageCountThreshold
        private set

    lateinit var from:Date
        private set

    lateinit var to:Date
        private set




    override fun apply(event: Event) {
        TODO("Not yet implemented")
    }
}