package domin.entity

import domin.valueObject.AccountId
import domin.valueObject.Money
import eventSystem.Event
import eventSystem.events.FinancialAccountWasCreatedForUserEvent
import eventSystem.events.MoneyDepositedToAccountEvent
import java.util.*

class Account private constructor(events: List<Event>):Entity(events)
{
    companion object
    {
        fun by(events:List<Event>):Account
        {
            return Account(events)
        }
    }

    lateinit var accountId:AccountId
        private set

    lateinit var userId:String
        private set

    lateinit var balance: Money
        private set


    constructor(userId: String):this(emptyList())
    {
        val accountId = UUID.randomUUID().toString()
        emitChange(FinancialAccountWasCreatedForUserEvent(userId,accountId))
    }

    fun deposit(money: Money) {
        emitChange(MoneyDepositedToAccountEvent(accountId.id,money.amount,money.unit.name))
    }

    override fun apply(event: Event) {
        when(event)
        {
            is FinancialAccountWasCreatedForUserEvent -> {
                accountId = AccountId(event.accountId)
                userId = event.userId
                balance = Money(0)
            }

            is MoneyDepositedToAccountEvent -> {
                balance += Money(event.amount,event.unit)
            }
        }
    }
}