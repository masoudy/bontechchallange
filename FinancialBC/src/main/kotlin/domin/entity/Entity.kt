package domin.entity

import eventSystem.Event
import eventSystem.EventStreamChangeList

abstract class Entity (val events:List<Event> = emptyList()){

    val expectedVersion:Int = events.size
    val changes:List<Event> = mutableListOf()

    init {
        events.forEach { apply(it) }
    }

    fun emitChange(event: Event)
    {
        (changes as MutableList<Event>).add(event)
        apply(event)
    }

    abstract fun apply(event: Event)


}