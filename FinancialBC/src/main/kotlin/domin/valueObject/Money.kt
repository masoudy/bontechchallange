package domin.valueObject

enum class MoneyUnit {
    Dollar,Euro,Rial
}

data class Money (val amount:Int,val unit: MoneyUnit = MoneyUnit.Rial)
{
    constructor(amount:Int, unit:String):this(amount, MoneyUnit.valueOf(unit))

    operator fun plus(money: Money):Money
    {
        return Money(amount + convert(money.unit,unit,money.amount) ,unit)
    }

    private fun convert(fromUnit: MoneyUnit , toUnit: MoneyUnit,amount: Int):Int
    {
        return -1;
    }
}