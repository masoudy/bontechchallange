package domin.repository

import domin.entity.Account
import domin.exception.ThereIsNoAccountForUser
import domin.valueObject.AccountId
import eventSystem.EventStore
import eventSystem.EventStreamName
import eventSystem.events.FinancialAccountWasCreatedForUserEvent

class AccountRepository (private val eventStore: EventStore){

    private fun eventStreamNameFor(id: AccountId) = "FinancialAccount-${id.id}"

    fun findAccountById(id:AccountId):Account
    {
        val streamName = EventStreamName(eventStreamNameFor(id))
        val stream = eventStore.fetchStream(streamName)
        val events = stream.allEvents()
        return Account.by(events)
    }

    fun findAccountByUserId(userId: String): Account
    {
        val accountId = findAccountIdForUserId(userId)
        return findAccountById(accountId)
    }

    private fun findAccountIdForUserId(userId:String):AccountId
    {
        val stream = eventStore.fetchStreamOf<FinancialAccountWasCreatedForUserEvent>()
        val event = stream.allEvents().firstOrNull{ (it as FinancialAccountWasCreatedForUserEvent).userId == userId}
        val accountId = (event as? FinancialAccountWasCreatedForUserEvent)?.accountId?.let { AccountId(it) }

        return  accountId ?: throw ThereIsNoAccountForUser(userId)
    }

    fun saveAndPublishChanges(account: Account) {
        val streamName = EventStreamName(eventStreamNameFor(account.accountId))
        eventStore.appendToStreamAndPublishChanges(streamName,account.expectedVersion,account.changes)
    }


}