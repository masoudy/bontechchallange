package usecase.depositToAccount

import domin.valueObject.Money


data class DepositToAccountRequestModel(val userId:String, val money: Money)