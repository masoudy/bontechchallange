package usecase.depositToAccount

import usecase.FinancialBCUsecaseHelper

class DepositToAccountUsecase {

    fun depositToAccount(rm:DepositToAccountRequestModel)
    {
        val repository = FinancialBCUsecaseHelper.accountRepository
        val account = repository.findAccountByUserId(rm.userId)
        account.deposit(rm.money)

        repository.saveAndPublishChanges(account)
    }

}