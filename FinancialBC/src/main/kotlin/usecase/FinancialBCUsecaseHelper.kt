package usecase

import domin.repository.AccountRepository
import domin.valueObject.Money
import eventSystem.Event
import eventSystem.EventStore
import eventSystem.EventStreamName
import eventSystem.commands.DepositMoneyToAccountCommand
import eventSystem.events.DepositingMoneyToAccountFailedEvent
import eventSystem.subscribeToPublishedEventsOfType
import usecase.depositToAccount.DepositToAccountRequestModel
import usecase.depositToAccount.DepositToAccountUsecase

object FinancialBCUsecaseHelper {

    private lateinit var eventStore: EventStore


    val accountRepository:AccountRepository by lazy {
        AccountRepository(eventStore)
    }

    fun initialize(eventStore: EventStore)
    {
        this.eventStore = eventStore

        subscribeOnCommandsWeAreInterestedIn()
    }

    private fun subscribeOnCommandsWeAreInterestedIn() {
        eventStore.subscribeToPublishedEventsOfType<DepositMoneyToAccountCommand>{

            val usecase = DepositToAccountUsecase()
            val rm = DepositToAccountRequestModel(it.userId, Money(it.amount,it.unit))

            try{
                usecase.depositToAccount(rm)
            }catch (e:Exception)
            {
                saveAndPublishCommandFailureEvent(DepositingMoneyToAccountFailedEvent(it.id))
            }
        }
    }

    private fun saveAndPublishCommandFailureEvent(event: Event)
    {
        val streamName = EventStreamName("CommandsStream")
        eventStore.appendToStreamAndPublishChanges(streamName,event)
    }

}